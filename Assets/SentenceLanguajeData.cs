﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SentenceLanguageData
{
    [SerializeField]
    private string text;
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    [SerializeField]
    private AudioClip clip;
    public AudioClip Clip
    {
        get { return clip; }
        set { clip = value; }
    }

    public SentenceLanguageData(string text,AudioClip audioClip)
    {
        this.text = text;
        this.clip = audioClip;
    }

}
