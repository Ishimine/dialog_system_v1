﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "DialogSys/SentenceLenguajeDataBank")]
[System.Serializable]
public class SentenceLenguajeDataBank : ScriptableObject
{
    private SentenceLenguajeDataDictionary bank = new SentenceLenguajeDataDictionary();
    public SentenceLenguajeDataDictionary Bank
    {
        get
        {
            return bank;
        }
    }

}


