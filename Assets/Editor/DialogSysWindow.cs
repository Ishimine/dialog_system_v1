﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DialogSysWindow : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    Color headerTextureColor = Color.cyan;

    Rect headerSection;
    Texture2D headerSectionTexture;

    Rect selectedCharacterSection;
    Texture2D selectedCharacterSectionTexture;

    private int selectedIndex = 0;

    private string characterKey = "None";

    private string[] currentCharacterSentencesIds;

    private SentenceLanguageData currentSentences;

    string logMessage;
    public MessageType currentMessageType;

    private SentencesManager sentencesManager;
    public SentencesManager SentencesManager
    {
        get
        {
            if (sentencesManager == null)
                FindSentencesManager();
            return sentencesManager;
        }
    }



    // serialize field on window so its value will be saved when Unity recompiles
    [SerializeField]
    Color m_Color = Color.white;











    // Add menu item named "My Window" to the Window menu
    [MenuItem("Window/DialogSysWindow")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(DialogSysWindow));
    }

    void OnGUI()
    {
        /*  GUILayout.Label("Base Settings", EditorStyles.boldLabel);
          myString = EditorGUILayout.TextField("Text Field", myString);

          groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
          myBool = EditorGUILayout.Toggle("Toggle", myBool);
          myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
          EditorGUILayout.EndToggleGroup();*/

        FindSentencesManager();
        DrawLayouts();
        DrawHeader();
        DrawSelectedCharacter();
    }

    private void FindSentencesManager()
    {
        sentencesManager = AssetDatabase.LoadAssetAtPath<SentencesManager>("Assets/DialogSys/_SentencesManager.asset");
        if(sentencesManager == null)
        {
            sentencesManager = ScriptableObject.CreateInstance<SentencesManager>();
            AssetDatabase.CreateAsset(sentencesManager, "Assets/DialogSys/_SentencesManager.asset");
            AssetDatabase.SaveAssets();
        }
    }

    private void OnEnable()
    {
        InitTextures();
    }

    void InitTextures()
    {
        headerSectionTexture = new Texture2D(1, 1);
        headerSectionTexture.SetPixel(0, 0, headerTextureColor);
        headerSectionTexture.Apply();
    }

    void DrawLayouts()
    {
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = Screen.width;
        headerSection.height = 50;
        GUI.DrawTexture(headerSection, headerSectionTexture);
        //  GUI.DrawTexture(headerSection, headerSectionTexture);

    }

    void DrawHeader()
    {
        //     selectedIndex = EditorGUILayout.Popup("Character: ", selectedIndex, masterIndex.Characters);
/*        GenericMenu genericMenu = new GenericMenu();

        string[] names = masterIndex.Characters;

        for (int i = 0; i < names.Length; i++)
        {
            AddMenuItemForColor(genericMenu, names[i], Color.gray);
        }



        GUILayout.BeginHorizontal();

        if(GUILayout.Button(characterKey) && names.Length > 0)
        {
            genericMenu.ShowAsContext();
        }

        if(GUILayout.Button("Create New"))
        {
            characterKey = "None";
            currentSentences = new SentenceDictionary();
        }

        if (characterKey != "None")
        {
            if (GUILayout.Button("Save"))
            {
                SaveCurrentSentence();
            }
        }

        GUILayout.EndHorizontal();
        */

       
    }

    private void SaveCurrentSentence()
    {
        logMessage = "";
        
    }

    // a method to simplify adding menu items
    void AddMenuItemForColor(GenericMenu menu, string menuPath, Color color)
    {
        // the menu item is marked as selected if it matches the current value of m_Color
        menu.AddItem(new GUIContent(menuPath), m_Color.Equals(color), OnCharacterSelected, color);
    }

    void OnCharacterSelected(object value)
    {
        string name = (string)value;
        currentCharacterSentencesIds = SentencesManager.GetCharacterSentenceIds(name);
    }

    void DrawSelectedCharacter()
    {
        selectedCharacterSection.x = 0;
        selectedCharacterSection.y = headerSection.height;
        selectedCharacterSection.width = Screen.width;
        selectedCharacterSection.height = 200;

        float height = selectedCharacterSection.y;

        Rect rect = new Rect(selectedCharacterSection.x,selectedCharacterSection.y, Screen.width, 20); height += 20;
  //      currentSentence.id = EditorGUI.DelayedTextField(rect, "ID: ", currentSentence.id);


    }
}