﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Character))]
public class CharacterEditor : Editor
{
    GUIStyle titleFont = new GUIStyle();

    private void OnEnable()
    {
        titleFont.fontSize = 20;
        titleFont.fontStyle = FontStyle.Bold;
    }

    public override void OnInspectorGUI()
    {
        Character myTarget = (Character)target;
        //DrawDefaultInspector();

        EditorGUILayout.LabelField(new GUIContent(myTarget.FullName), titleFont, GUILayout.Height(35));

        myTarget.Name = EditorGUILayout.TextField("Name:", myTarget.Name);
        myTarget.Surname = EditorGUILayout.TextField("Surname:", myTarget.Surname);

      /*  foreach (string k in myTarget.SentencesKeys)
        {
            var aux = myTarget.sentences[k];

            aux.audioKey = EditorGUILayout.Popup("AudioKey: ", aux.audioKey, myTarget.AudioKeys);
            aux.expresionKey = EditorGUILayout.Popup("ExpresionKey: ", aux.expresionKey, myTarget.ExpresionKeys);
            //aux.textKey = EditorGUILayout.Popup("TextKey: ", aux.textKey, myTarget.TextKeys);

        }
        */

    }
}
