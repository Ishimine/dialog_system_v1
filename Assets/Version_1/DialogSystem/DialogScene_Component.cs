﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DialogScene_Component : MonoBehaviour
{
    public UI_SentenceOutput_Component[] outputs;

    private DialogScene dialogScene;
    public DialogScene DialogScene
    {
        get
        {
            return dialogScene;
        }
    }

    public bool autoPlay;

    private void Awake()
    {
        UI_SentenceOutput[] uI_SentenceOutputs = new UI_SentenceOutput[outputs.Length];
        for (int i = 0; i < outputs.Length; i++)
        {
            uI_SentenceOutputs[i] = outputs[i].uI_SentenceOutput;
        }
        dialogScene = new DialogScene(uI_SentenceOutputs, autoPlay);
    }

    [Button]
    public void SendText(IDTextFull dTextFull)
    {
        dialogScene.ShowText(dTextFull);
    }





}
