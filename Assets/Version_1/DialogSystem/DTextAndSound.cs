﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DTextAndSound : IDTextAndSound
{
    [SerializeField]
    private DText dText;

    [SerializeField]
    private AudioClip clip;
    public AudioClip Clip
    {
        get { return clip; }
    }

    public string SourceId
    {
        get
        {
            return dText.SourceId;
        }
    }
    public string SourceName
    {
        get
        {
            return dText.SourceName;
        }
    }
    public string Text
    {
        get
        {
            return dText.Text;
        }
    }
}
