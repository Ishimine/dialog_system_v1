﻿using UnityEngine;

public interface IDTextAndSound : IDText
{
     AudioClip Clip
    {
        get;
    }
}
