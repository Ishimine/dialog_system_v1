﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISentenceOutPut
{
    string Id
    {
        get;
    }



    void Display(IDTextFull text);

}