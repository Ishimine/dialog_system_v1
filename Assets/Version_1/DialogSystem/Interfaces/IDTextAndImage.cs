﻿using UnityEngine;

public interface IDTextAndImage : IDText
{
    Sprite Sprite
    {
        get;
    }
}
