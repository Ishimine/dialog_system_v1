﻿public interface IDText
{
    string SourceId
    {
        get;
    }

    string SourceName
    {
        get;
    }

    string Text
    {
        get;
    }
}
