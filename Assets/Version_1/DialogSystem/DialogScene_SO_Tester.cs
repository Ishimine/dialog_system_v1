﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DialogScene_SO_Tester : MonoBehaviour
{
    public DialogScene_Component dialogScene_Component;

    public SO_Sentence[] sentences;

    [Button]
    public void SendAllSentences()
    {
        for (int i = 0; i < sentences.Length; i++)
        {
            dialogScene_Component.DialogScene.ShowText(sentences[i].sentence);
        }
    }
}
