﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DTextFull : IDTextFull
{
    [SerializeField]
    private Sprite sprite;
    public Sprite Sprite
    {
        get
        {
            return sprite;
        }
    }

    [SerializeField]
    private AudioClip audioClip;
    public AudioClip Clip
    {
        get
        {
            return audioClip;
        }
    }

    [SerializeField]
    private DText dText;
    public string SourceId
    {
        get
        {
            return dText.SourceId;
        }
    }
    public string SourceName
    {
        get
        {
            return dText.SourceName;
        }
    }
    public string Text
    {
        get
        {
            return dText.Text;
        }
    }
}
