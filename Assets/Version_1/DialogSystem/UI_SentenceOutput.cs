﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UI_SentenceOutput : SentenceOutPut
{
    public RectTransform container;
    public Image image;
    public Text text;

    private CodeAnimator transitionAnimator = new CodeAnimator();
    private CodeAnimator textAnimator = new CodeAnimator();

    [Min(0)]
    public float percentageDistanceFromBorder;

    public enum AnimationDirection {Up,Down,Left,Right }

    [SerializeField]
    private AnimationDirection direction = AnimationDirection.Down;
    private Vector2Int GetDirection()
    {
        switch (direction)
        {
            case AnimationDirection.Up:
                return Vector2Int.up;
            case AnimationDirection.Down:
                return Vector2Int.down;
            case AnimationDirection.Left:
                return Vector2Int.left;
            case AnimationDirection.Right:
                return Vector2Int.right;
            default:
                return Vector2Int.zero;
        }
    }



    public float charPerSecond = 5;

    public CodeAnimatorCurve curveIn;
    public CodeAnimatorCurve curveOut;

    public override void Skip()
    {
        text.text = CurrentSentence.Text;
        OnDisplayDone?.Invoke();
    }

    protected override void Clear_Sandbox()
    {
        text.text = "";
        image.sprite = null;
        image.color = Color.clear;
    }

    protected override void TransitoinIn(Action postAction)
    {
        image.color = Color.white;
        image.sprite = CurrentSentence.Sprite;
        Vector2 dir = GetDirection();
        float dist = container.rect.height  + percentageDistanceFromBorder * container.rect.height;
        transitionAnimator.StartAnimacion(MonoProxy,
            x =>
            {
                container.anchoredPosition = dir * x * dist;
            },
            DeltaTimeType.deltaTime, AnimationType.Inverse, curveIn.Curve,
            postAction, curveIn.Time);
    }

    protected override void TransitoinOut(Action postAction)
    {
       // float startDist = container.rect.height  + percentageDistanceFromBorder * container.rect.height;
        float dist = /*startDist +*/ container.rect.height;
        Vector2 dir = GetDirection();
        transitionAnimator.StartAnimacion(
       MonoProxy,
       x =>
       {
           container.anchoredPosition = /*dir * startDist -*/ dir * x * dist;
       }, DeltaTimeType.deltaTime, AnimationType.Simple, curveOut.Curve,
       postAction
       , curveOut.Time);
    }

    protected override void DisplaySentence_Sandbox(IDTextFull text)
    {
        image.sprite = CurrentSentence.Sprite;

        if(text.Text == "" || text.Text.Length == 0)
        {
            this.text.text = "";
            OnDisplayDone?.Invoke();
            return;
        }

        float duration = text.Text.Length / charPerSecond;
        int l = text.Text.Length;
        textAnimator.StartAnimacion
            (MonoProxy,
            x =>
            {
                this.text.text = text.Text.Substring(0, (int)Mathf.Lerp(0, text.Text.Length, x)); 
            }, DeltaTimeType.deltaTime, AnimationType.Simple, null, 
            ()=> 
            {
                OnDisplayDone?.Invoke();
            }, duration
            );
    }
}

