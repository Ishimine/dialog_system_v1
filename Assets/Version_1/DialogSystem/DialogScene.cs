﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class DialogScene
{
    [SerializeField]
    private bool autoPlay = false;
    public bool AutoPlay
    {
        get { return autoPlay; }
        set { autoPlay = value; }
    }

    [SerializeField]
    private bool isDisplaying = false;
    public bool IsDisplaying
    {
        get { return isDisplaying; }
        set { isDisplaying = value; }
    }

    public Queue<IDTextFull> queue;

    private SentenceOutPut[] outputs;
    public SentenceOutPut[] Outputs
    {
        get
        {
            return outputs;
        }
        set
        {
            outputs = value;
        }
    }

    private SentenceOutPut currentOutPut;
    protected SentenceOutPut CurrentOutPut
    {
        get
        {
            return currentOutPut;
        }
        set
        {
            currentOutPut = value;
        }
    }

    protected IDTextFull currentIDTextFull;

    #region Events

    private VoidEvent onSentenceDisplayStarted;
    public VoidEvent OnSentenceDisplayStarted
    {
        get { return onSentenceDisplayStarted; }
        set { onSentenceDisplayStarted = value; }
    }

    private VoidEvent onSentenceDisplayDone;
    public VoidEvent OnSentenceDisplayDone
    {
        get { return onSentenceDisplayDone; }
        set { onSentenceDisplayDone = value; }
    }

    #endregion

    private ReplacementStrategy currentReplacementStrategy = new Farthest();
    public ReplacementStrategy CurrentReplacementStrategy
    {
        get
        {
            return currentReplacementStrategy;
        }
    }
        
    #region Constructores

    public DialogScene(SentenceOutPut[] OutPuts, bool autoPlay)
    {
        this.autoPlay = autoPlay;
        queue = new Queue<IDTextFull>();
        this.Outputs = OutPuts;
        Initialize();
    }
        
    private void Initialize()
    {
        isDisplaying = false;
        for (int i = 0; i < Outputs.Length; i++)
        {
            Outputs[i].OnDisplayStarted -= OnDisplayStarted;
            Outputs[i].OnDisplayDone -= OnDisplayDone;

            Outputs[i].OnDisplayStarted += OnDisplayStarted;
            Outputs[i].OnDisplayDone += OnDisplayDone;
        }
    }

    #endregion

    public void Skip()
    {
        CurrentOutPut.Skip();
    }

    private void OnDisplayStarted()
    {
        IsDisplaying = true;
        OnSentenceDisplayStarted?.Invoke();
    }

    private void OnDisplayDone()
    {
        Debug.Log("DialogScene => OnDisplayDone");
        IsDisplaying = false;

        OnSentenceDisplayDone?.Invoke();
        if (AutoPlay)
        {
            ShowNext();
        }
        else
            IsDisplaying = false;
    }

    public void ShowText(IDTextFull txt)
    {
        AddText(txt);
        ShowNext();
    }

    public void AddText(IDTextFull txt)
    {
        queue.Enqueue(txt);
    }

    public void RemoveText(IDTextFull txt)
    {
        queue.Enqueue(txt);
    }

    [Button]
    public void ShowNext()
    {
        if (isDisplaying || queue.Count == 0) return;

        Debug.Log("ShowNext");
        currentIDTextFull = queue.Dequeue();

        Debug.Log("currentIDTextFull: " + currentIDTextFull);

        CurrentOutPut = FindOutPut(currentIDTextFull.SourceId);
        if (CurrentOutPut == null)      //El participante del dialogo activo NO se encuentra en pantalla, Hay que reemplazar a alguno de los que ya esta
        {
            CurrentOutPut = GetReplaceVictim();
        }

       // Debug.Log("CurrentOutPut: " + CurrentOutPut);
       // Debug.Log("DisplaySentence: " + CurrentOutPut.SourceId);

        CurrentOutPut.DisplaySentence(currentIDTextFull);
    }

    private SentenceOutPut GetReplaceVictim()
    {
       return CurrentReplacementStrategy.GetVictim(this);
    }

    private void ReplaceParticipant(SentenceOutPut sentenceOutPut, IDTextFull dTextFull)
    {

    }
    /// <summary>
    /// Busca la salida de sentencia, 
    /// </summary>
    /// <param name="id">ID del output</param>
    private SentenceOutPut FindOutPut(string id)
    {
        for (int i = 0; i < Outputs.Length; i++)
        {
            if (Outputs[i].SourceId == id)
            {
                Debug.Log("Found: " + Outputs[i].SourceId);
                return Outputs[i];
            }
        }
        return null;
    }

}


public abstract class ReplacementStrategy
{
    public abstract SentenceOutPut GetVictim(DialogScene dialogScene);
}

/// <summary>
/// Busca el participante que MAS lejos se encuentra en la Fila de dialogos
/// </summary>
public class Farthest : ReplacementStrategy
{
    List<IDTextFull> sentences;
    SentenceOutPut[] currentOutputs;
    List<SentenceOutPut> currentParticipants;

    private int delta;
    /// <summary>
    /// Define cuanto en el futuro se considera para el reemplazo.
    /// </summary>
    public int Delta
    {
        get { return delta; }
    }

    public Farthest()
    {
        delta = int.MaxValue;
    }

    public Farthest(int delta)
    {
        if(delta == -1)
        {
            delta = int.MaxValue;
        }
    }

    public override SentenceOutPut GetVictim(DialogScene dialogScene)
    {
        SentenceOutPut aOutput = null;
        currentOutputs = new SentenceOutPut[dialogScene.Outputs.Length];
        dialogScene.Outputs.CopyTo(currentOutputs,0);
        sentences = new List<IDTextFull>(dialogScene.queue);       //Frases que quedan por decir
        currentParticipants = new List<SentenceOutPut>(currentOutputs);


        ///Busca algun output vacio
        aOutput = currentParticipants.Find(x => x.IsEmpty);
        if (aOutput != null) return aOutput;



        IDTextFull aSentence;                       //variable auxiliar iteracion
        int maxDistance = 0;
        int currentDistance;
        for (int i = 0; i < currentOutputs.Length; i++)
        {
            aSentence = sentences.Find(x => x.SourceId == currentOutputs[i].SourceId);

            if (aSentence == null)
                return currentOutputs[i];
            else 
            {
                currentDistance = sentences.IndexOf(aSentence);
                if(maxDistance < currentDistance)
                {
                    maxDistance = currentDistance;
                    aOutput = currentOutputs[i];
                }
            }
        }
        return aOutput;
    }

    private SentenceOutPut GetEmptyOutput(SentenceOutPut[] outputs)
    {
        for (int i = 0; i < outputs.Length; i++)
        {
            if (outputs[i].IsEmpty)
                return outputs[i];
        }
        return null;
    }


}