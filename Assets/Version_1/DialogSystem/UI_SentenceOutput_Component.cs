﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_SentenceOutput_Component : MonoBehaviour
{
    public UI_SentenceOutput uI_SentenceOutput;

    private void Awake()
    {
        uI_SentenceOutput.Clear();
    }
}
