﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DialogeSystem/Sentence")]
public class SO_Sentence : ScriptableObject
{
    public DTextFull sentence;
}
