﻿using UnityEngine;

[System.Serializable]
public struct DTextAndImage : IDTextAndImage
{
    [SerializeField]
    private DText dText;

    [SerializeField]
    private Sprite sprite;
    public Sprite Sprite
    {
        get { return sprite; }
    }

    public string SourceId
    {
        get
        {
            return dText.SourceId;
        }
    }


    public string SourceName
    {
        get
        {
            return dText.SourceName;
        }
    }
    public string Text
    {
        get
        {
            return dText.Text;
        }
    }
}
