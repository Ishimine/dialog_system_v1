﻿using UnityEngine;

[System.Serializable]
public struct DText : IDText
{
    [SerializeField]
    private string source;
    public string SourceId
    {
        get
        {
            return source;
        }   
    }

    [SerializeField]
    private string sourceName;
    public string SourceName
    {
        get
        {
            return sourceName;
        }
    }

    [SerializeField, TextArea(3, 10)]
    private string text;
    public string Text
    {
        get
        {
            return text;
        }
    }
}
