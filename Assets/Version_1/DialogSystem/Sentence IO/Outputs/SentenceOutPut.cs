﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[System.Serializable]
public abstract class SentenceOutPut
{
    private MonoProxy monoProxy = null;
    public MonoBehaviour MonoProxy
    {
        get
        {
            if (monoProxy == null)
                monoProxy = new MonoProxy();
            return monoProxy.MonoBehaviourProxy;
        }
    }

    public bool IsEmpty
    {
        get { return SourceId == ""; }
    }

     public string SourceId
    {
        get
        {
            if(currentSentence == null)
            {
                return "";
            }
            return currentSentence.SourceId;
        }
    }

    private VoidEvent onDisplayStarted;
    public VoidEvent OnDisplayStarted
    {
        get { return onDisplayStarted;  }
        set { onDisplayStarted = value; }
    }

    private VoidEvent onDisplayDone;
    public VoidEvent OnDisplayDone
    {
        get { return onDisplayDone;    }
        set { onDisplayDone = value;   }
    }

    [SerializeField]
    private IDTextFull currentSentence;
    public IDTextFull CurrentSentence
    {
        get { return currentSentence; }
    }


    /// <summary>
    /// Displays with animation
    /// </summary>
    /// <param name="nText"></param>
    [Button]
    public void DisplaySentence(IDTextFull nText)
    {
        OnDisplayStarted?.Invoke();
       
        if(SourceId != "" && SourceId != nText.SourceId)     //El nuevo dialogo es de alguien diferente al actual;
        {
            ReplaceAndDisplay(nText);
        }
        else if(SourceId == "")                             //El dialogo esta vacio
        {
            currentSentence = nText;
            TransitoinIn(()=> DisplaySentence_Sandbox(nText));
        }
        else                                                //El dialogo pertenece al mismo Source_ID
        {
            currentSentence = nText;
            DisplaySentence_Sandbox(nText);
        }
    }

    /// <summary>
    /// Necesita sacar los elementos de dialogo actuales. y luego llamar a DisplaySentence_Sandbox para mostrar los actuales
    /// </summary>
    /// <param name="nText"></param>
    private void ReplaceAndDisplay(IDTextFull nText)
    {
        Action action = () =>
        {
            currentSentence = nText;
            TransitoinIn(()=> DisplaySentence_Sandbox(nText));
        };
        TransitoinOut(action);
    }

    protected abstract void DisplaySentence_Sandbox(IDTextFull text);

    protected abstract void TransitoinIn(Action postAction);
    protected abstract void TransitoinOut(Action postAction);

    /// <summary>
    /// Apply instantly
    /// </summary>
    /// <param name="text"></param>
    public abstract void Skip();

    public void Clear()
    {
        currentSentence = null;
        Clear_Sandbox();
    }

    protected abstract void Clear_Sandbox();

}
