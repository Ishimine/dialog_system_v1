﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagExample : MonoBehaviour
{
    [DropDownList]
    public string TagFilter = "";

    [DropDownList]
    public string[] TagFilterArray = new string[] { };
}
