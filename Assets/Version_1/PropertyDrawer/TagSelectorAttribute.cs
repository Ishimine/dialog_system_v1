﻿using UnityEngine;

public class DropDownListAttribute : PropertyAttribute
{
    public bool UseDefaultTagFieldDrawer = false;
}
