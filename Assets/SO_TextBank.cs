﻿using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[CreateAssetMenu(menuName =("DialogSys/Bank/Text"))]
[System.Serializable]
public class SO_TextBank : ScriptableObject
{
    public TextBank bank;
}

[System.Serializable]
public class TextBank : GenericBank<string>
{
    public TextDictionary data; 
    protected override SerializableDictionaryBase<string, string> Data
    {
        get
        {
            return data;
        }
    }
}


