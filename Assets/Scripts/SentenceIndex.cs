﻿using System;

[Serializable]
public class SentenceIndex
{
   // public Character character;
    public string id;
    public string characterKey;
    public string expresionKey;
    public float startPause;

    public SentenceIndex(string id, string characterKey, string expresionKey,  float startPause)
    {
        this.id = id;
        this.characterKey = characterKey;
        this.expresionKey = expresionKey;
        this.startPause = startPause;
    }

}

