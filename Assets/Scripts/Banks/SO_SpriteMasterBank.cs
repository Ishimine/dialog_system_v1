﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "DialogSys/Bank/_MasterSprites"), System.Serializable]
public class SO_SpriteMasterBank : ScriptableObject
{
    public SpriteMasterBank bank;
}
