﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class SpriteMasterBank : GenericMasterBank<SO_SpriteBank>
{

}

[System.Serializable]
public class SpriteDictionary : SerializableDictionaryBase<string, Sprite> { }

