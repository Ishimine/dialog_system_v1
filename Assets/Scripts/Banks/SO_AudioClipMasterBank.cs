﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "DialogSys/Bank/_MasterAudioClips"), System.Serializable]
public class SO_AudioClipMasterBank : ScriptableObject
{
    public AudioClipMasterBank Bank;
}
