﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class TextMasterBank : GenericMasterBank<SO_TextBank>
{

}

[System.Serializable]
public class TextDictionary : SerializableDictionaryBase<string, string> { }