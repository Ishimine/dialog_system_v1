﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;


[System.Serializable]
public class AudioClipMasterBank : GenericMasterBank<SO_AudioClipBank>
{
  
}

[System.Serializable]
public class AudioClipDictionary : SerializableDictionaryBase<string, AudioClip> { }