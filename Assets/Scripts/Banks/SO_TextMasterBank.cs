﻿using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName = "DialogSys/Bank/_MasterText"), System.Serializable]
public class SO_TextMasterBank : ScriptableObject
{
    public TextMasterBank bank;
}
