﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;


public class SentencesManager : ScriptableObject
{

    private List<string> languages;
    public List<string> Languages
    {
        get
        {
            return languages;
        }
    }

    public string currentLanguage;
    public string CurrentLanguaje
    {
        get
        {
            if(currentLanguage == "")
            {
                currentLanguage = languages[0];
            }
            return currentLanguage;
        }
    }

    [SerializeField]
    private SentenceLangDataBankDictionary langData;
    public SentenceLangDataBankDictionary LangData
    {
        get { return langData; }
        set { langData = value; }
    }

    [SerializeField]
    private SentenceIndexDictionary constantData;
    public SentenceIndexDictionary ConstantData
    {
        get { return constantData; }
        set { constantData = value; }
    }

    [SerializeField]
    private CharacterDataBase characterDB;
    public CharacterDataBase CharacterDB
    {
        get
        {
            return characterDB;
        }
    }

    public bool GetSentence(string id, ref Sentence sentence, string language = "")
    {
        if (language == "")
            language = CurrentLanguaje;
        SentenceIndex sentenceIndex;
        SentenceLanguageData sentenceLanguageData;

        if (!ConstantData.ContainsKey(id))
        {
            Debug.Log("ID: " + id + " not found.");
            return false;
        }
        else if(!langData[language].Bank.ContainsKey(id))
        {
            Debug.Log("ID: " + id + " not found.");
            return false;
        }
        else
        {
            sentenceIndex = constantData[id];
            sentenceLanguageData = langData[language].Bank[id];
            Character c = characterDB.GetCharacter(sentenceIndex.characterKey);
            sentence = new Sentence(
                sentenceLanguageData.Text, 
                sentenceLanguageData.Clip, 
                sentenceIndex.characterKey, 
                sentenceIndex.startPause, 
                c.Name, 
                c.ExpressionSprites
                [sentenceIndex.expresionKey]);
            return true;
        }

    }

    private void AddLanguageSentence(string id, string language, SentenceLanguageData sentence)
    {
        if (!langData[language].Bank.ContainsKey(id))
        {
            langData[language].Bank.Add(id, sentence);
        }
        else
        {
            Debug.LogWarning("Bank of Language " + language + " not found.");
        }
    }

    private void RemoveLanguageSentence(string id, string language)
    {
        if (langData[language].Bank.ContainsKey(id))
        {
            langData[language].Bank.Remove(id);
        }
    }

    private void AddConstantSentence(string id, SentenceIndex sentenceIndex, bool overrideCurrent = false)
    {
        if (!constantData.ContainsKey(id))
        {
            constantData.Add(id, sentenceIndex);
        }
        else if (!overrideCurrent)
            Debug.LogWarning("El id: " + id + " y esta en uso.");
        else
            Debug.LogWarning("El id: " + id + " a sido sobreescribido.");
    }

    private void RemoveConstantSentence(string id)
    {
        if (!constantData.ContainsKey(id))
        {
            constantData.Remove(id);
        }
    }

    public void AddSentence(string id, string language, SentenceLanguageData sentence, SentenceIndex sentenceIndex)
    {
        if(!languages.Contains(language))
        {
            Debug.LogError("Languaje: " + langData + "  not found in Languaje List");
        }
        else if(!langData.ContainsKey(language))
        {
            Debug.LogError("Languaje banck: " + langData + "  not found in Languaje List");
        }
        else
        {
            langData[language].Bank.Add(id, sentence);
            constantData.Add(id, sentenceIndex);
        }
    }

    public void Remove(string id, string language)
    {
        if (!languages.Contains(language))
        {
            Debug.LogError("Languaje: " + langData + "  not found in Languaje List");
        }
        else if (!langData.ContainsKey(language))
        {
            Debug.LogError("Languaje banck: " + langData + "  not found in Languaje List");
        }
        else
        {
            RemoveLanguageSentence(id, language);
            RemoveConstantSentence(id);
        }
    }

    public string[] GetCharacterSentenceIds(string characterKey)
    {
        List<SentenceIndex> l = new List<SentenceIndex>(constantData.Values);
        List<string> rValue = new List<string>();
        l.FindAll(x => x.characterKey == characterKey);
        for (int i = 0; i < l.Count; i++)
        {
            rValue.Add(l[i].id);
        }
        return rValue.ToArray();
    }
}


public class SentenceIndexDictionary : SerializableDictionaryBase<string, SentenceIndex> { }
