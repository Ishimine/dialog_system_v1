﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[CreateAssetMenu(menuName = "DialogSys/Character")]
public class Character : ScriptableObject
{
    private int uniqueId;
    public int UniqueId
    {
        get { return uniqueId; }
    }

    [SerializeField]
    private string name;
    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    [SerializeField]
    private string surname;
    public string Surname
    {
        get { return surname; }
        set { surname = value; }
    }

    public string FullName
    {
        get
        {
            return name + " " + surname;
        }
    }

    private string[] expresionKeys;
    public string[] ExpresionKeys
    {
        get
        {
            if (IsDirty) UpdateExpresionKeys();
            return expresionKeys;
        }
    }

    private bool isDirty = false;
    public bool IsDirty
    {
        get { return isDirty; }
        set { isDirty = value; }
    }

    private SpriteDictionary expressionSprites;
    public SpriteDictionary ExpressionSprites
    {
        get { return expressionSprites; }
        set { expressionSprites = value; }
    }

    public void Initialize(CharacterDataBase characterDataBase)
    {
        characterDataBase.AddCharacter(this);
    }

    private void UpdateExpresionKeys()
     {
         string[] expresionKeys = new string[ExpressionSprites.Keys.Count];
         ExpressionSprites.Keys.CopyTo(expresionKeys, 0);
     }


    /*   private List<Sentence> sentences;
       public List<Sentence> Sentences
       {
           get { return sentences; }
           set { sentences = value; }
       }
       */
    /*

  
    private string[] audioKeys;
    public string[] AudioKeys
    {
        get
        {
            if (IsDirty) UpdateKeys();
            return audioKeys;
        }
    }


    private string[] sentencesKeys;
    public string[] SentencesKeys
    {
        get
        {
            if (IsDirty) UpdateKeys();
            return sentencesKeys;
        }
    }

 

     private void UpdateKeys()
     {
         UpdateAudioKeys();
         UpdateExpresionKeys();
         UpdateSentencesKeys();
         IsDirty = false;
     }


     private void UpdateAudioKeys()
     {
         string[] audioKeys = new string[audioClips.Keys.Count];
         audioClips.Keys.CopyTo(audioKeys, 0);
     }

     private void UpdateExpresionKeys()
     {
         string[] expresionKeys = new string[expresions.Keys.Count];
         expresions.Keys.CopyTo(expresionKeys, 0);
     }

     private void UpdateSentencesKeys()
     {
         string[] sentencesKeys = new string[sentences.Keys.Count];
         sentences.Keys.CopyTo(sentencesKeys, 0);
     }(-)*/
}


