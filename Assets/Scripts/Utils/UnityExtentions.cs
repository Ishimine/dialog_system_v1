﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityExtentions 
{

    /// <summary>
    /// Extension method to check if a layer is in a layermask
    /// </summary>
    /// <param name="mask"></param>
    /// <param name="layer"></param>
    /// <returns></returns>
    public static bool Contains(this LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    public static Vector2 GetDirection(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos,endPos).normalized;
    }

    public static Vector3 GetDirection(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference(startPos, endPos).normalized;
    }

    public static float GetMagnitud(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos, endPos).magnitude;
    }

    public static float GetMagnitud(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference(startPos, endPos).magnitude;
    }

    public static float GetMagnitud(this Vector3 startPos, Vector2 endPos)
    {
        return GetDifference((Vector2)startPos,endPos).magnitude;
    }

    public static float GetSqrMagnitud(this Vector2 startPos, Vector2 endPos)
    {
        return GetDifference(startPos, endPos).sqrMagnitude;
    }

    public static float GetSqrMagnitud(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference((Vector2)startPos, (Vector2)endPos).sqrMagnitude;
    }

    public static Vector2 GetDifference(this Vector2 startPos, Vector2 endPos)
    {
        return (endPos - startPos);
    }

    public static Vector3 GetDifference(this Vector3 startPos, Vector3 endPos)
    {
        return GetDifference((Vector2)startPos, (Vector2)endPos);
    }

    public static float GetAngle(this Vector2 startPos)
    {
        Vector2 dif = startPos.normalized;
        float angle = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
        return angle;
    }


    public static float GetAngle(this Vector2 startPos, Vector2 endPos)
    {
        Vector2 dif = GetDirection(startPos, endPos);
        float angle = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
        return angle;
    }

    public static float GetAngle(this Vector3 startPos, Vector3 endPos)
    {
        return GetAngle((Vector2)startPos, (Vector2)endPos);
    }

    public static Vector2 GetRandomBetween(this Vector2 startPos, Vector3 endpos)
    {
        return Vector2.Lerp(startPos, endpos, Random.Range(0, 1));
    }

    public static Vector2 GetRandomBetweenAsRect(this Vector2 startPos, Vector3 endpos)
    {
        return new Vector2(Mathf.Lerp(startPos.x, endpos.x, Random.Range(0f,1f)), Mathf.Lerp(startPos.y, endpos.y, Random.Range(0f, 1f)));
    }

    public static Vector2 ClampPositionToView(this Camera camera, Vector2 value)
    {
        float hSize = GetHorizontalSize(camera);
        return new Vector2(Mathf.Clamp(value.x, -hSize, hSize), Mathf.Clamp(value.y, -camera.orthographicSize, camera.orthographicSize)) + (Vector2)camera.transform.position;
    }

    public static Vector2 Clamp(this Vector2 value, Vector2 min, Vector2 max)
    {
        return new Vector2(Mathf.Clamp(value.x, min.x, max.x), Mathf.Clamp(value.y, min.y, max.y));
    }

    public static bool IsInsideCameraView(this Camera camera, Vector2 value)
    {
        value -= (Vector2)camera.transform.position;
        return Mathf.Abs(value.y) < camera.orthographicSize && Mathf.Abs(value.x) < camera.GetHorizontalSize();
    }

    public static float GetHorizontalSize(this Camera camera)
    {
        return camera.aspect * camera.orthographicSize * 2;
    }


    public static bool ContainsInLocalSpace(this BoxCollider2D boxCollider2D, Vector2 worldSpacePoint)
    {
        worldSpacePoint = boxCollider2D.transform.InverseTransformPoint(worldSpacePoint);
        return Mathf.Abs(worldSpacePoint.x) <= boxCollider2D.size.x/2 && Mathf.Abs(worldSpacePoint.y) <= boxCollider2D.size.y/2;
    }


    public static Vector2 RadianToVector2(this float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    public static Vector2 DegreeToVector2(this float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }


    public static T FindInRoot<T>(this Transform source)
    {
        Transform t = source;
        while(t.parent != null)
        {
            t = t.parent;
        }
        return t.GetComponent<T>();
    }
}


public delegate void V2Event(Vector2 value);
public  delegate void IntEvent(int value);
public  delegate void FloatEvent(float value);
public delegate void VoidEvent();
