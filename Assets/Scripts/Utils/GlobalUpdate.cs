﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalUpdate : MonoBehaviour
{
    private static VoidEvent globalUpdateEvent;
    public static VoidEvent GlobalUpdateEvent
    {
        get { return globalUpdateEvent; }
        set { globalUpdateEvent = value; }
    }

    private static VoidEvent globalFixedUpdateEvent;
    public static VoidEvent GlobalFixedUpdateEvent
    {
        get { return globalFixedUpdateEvent; }
        set { globalFixedUpdateEvent = value; }
    }

    private static VoidEvent globalLateUpdateEvent;
    public static VoidEvent GlobalLateUpdateEvent
    {
        get { return globalLateUpdateEvent; }
        set { globalLateUpdateEvent = value; }
    }

    private static GlobalUpdate instance = null;

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        globalUpdateEvent = null;
        globalFixedUpdateEvent = null;
        globalLateUpdateEvent = null;
    }

    private void Update()
    {
        GlobalUpdateEvent?.Invoke();
    }

    private void LateUpdate()
    {
        GlobalLateUpdateEvent?.Invoke();
    }

    private void FixedUpdate()
    {
        GlobalFixedUpdateEvent?.Invoke();
    }
}
