﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoProxy
{
    private MonoBehaviour monoBehaviour;
    public MonoBehaviour MonoBehaviourProxy
    {
        get { return monoBehaviour; }
    }

    public MonoProxy()
    {
        monoBehaviour = new GameObject().AddComponent<Empty>();
    }

}
