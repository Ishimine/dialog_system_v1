﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(menuName =("DialogSys/CharacterDataBase"))]
public class CharacterDataBase : ScriptableObject
{
    public CharacterDictionary characters = new CharacterDictionary();

    public Character GetCharacter(string uniqueId)
    {
        if(characters.ContainsKey(uniqueId))
        {
            return characters[uniqueId];
        }
        else
        {
            return null;
        }
    }

    public bool AddCharacter(Character nChar)
    {
        if (characters.ContainsValue(nChar))
        {
            return false;
        }
        else
        {
            int randomId;
            do
            {
                randomId = Random.Range(0, int.MaxValue);
            } while (characters.ContainsKey(randomId.ToString()));
            characters.Add(randomId.ToString(), nChar);
            return true;
        }
    }

    public bool RemoveCharacter(Character nChar)
    {
        return RemoveCharacter(nChar.FullName);
    }

    public bool RemoveCharacter(string key)
    {
        if (characters.ContainsKey(key))
        {
            return false;
        }
        else
        {
            characters.Remove(key);
            return true;
        }
    }




}

[System.Serializable]
public class CharacterDictionary : SerializableDictionaryBase<string, Character> { }