﻿using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public abstract class GenericBank<T> 
{
    [SerializeField]
    protected abstract SerializableDictionaryBase<string, T> Data
    {
        get;
    }

    public T GetValue(string key)
    {
        if (!Data.ContainsKey(key))
        {
            return default;
        }
        else
        {
            return Data[key];
        }
    }

    public bool Contains(string key)
    {
        return Data.ContainsKey(key);
    }
}