﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Sentence
{
    [SerializeField]
    private string text;
    public string Text
    {
        get { return text; }
        set { text = value; }
    }


    [SerializeField]
    private Sprite sprite;
    public Sprite Sprite
    {
        get { return sprite; }
        set { sprite = value; }
    }

    [SerializeField]
    private AudioClip clip;
    public AudioClip Clip
    {
        get { return clip; }
        set { clip = value; }
    }

    [SerializeField]
    private string characterName;
    public string CharacterName
    {
        get { return characterName; }
        set { characterName = value; }
    }

    [SerializeField]
    private float startPause;
    public float StartPause
    {
        get { return startPause; }
        set { startPause = value; }
    }

    public Sentence(string text, AudioClip audioClip, string characterKey, float startPause, string characterName, Sprite sprite)
    {
        this.text = text;
        this.characterName = characterName;
        this.clip = audioClip;
        this.sprite = sprite;
        this.startPause = startPause;
    }


}
