﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[CreateAssetMenu(menuName =("DialogSys/Bank/Sprite"))]
[System.Serializable]
public class SO_SpriteBank : SO_GenericBank<Sprite>
{
    public SpriteBank bank;
}

[System.Serializable]
public class SpriteBank : GenericBank<Sprite>
{
    public SpriteDictionary data;
    protected override SerializableDictionaryBase<string, Sprite> Data
    {
        get
        {
            return data;
        }
    }
        
}