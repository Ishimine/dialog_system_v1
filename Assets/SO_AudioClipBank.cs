﻿using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[CreateAssetMenu(menuName =("DialogSys/Bank/AudioClip"))]
[System.Serializable]
public class SO_AudioClipBank : SO_GenericBank<AudioClip>
{
    public AudioClipBank bank;
}

[System.Serializable]
public class AudioClipBank : GenericBank<AudioClip>
{
    public AudioClipDictionary data;
    protected override SerializableDictionaryBase<string, AudioClip> Data
    {
        get
        {
            return data;
        }
    }
}